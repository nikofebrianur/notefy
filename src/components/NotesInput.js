import React from "react";

class NotesInput extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			title: '',
			body: '',
			titleLength: 0
		}

		this.onTitleChangeEventHandler = this.onTitleChangeEventHandler.bind(this);
		this.onContentChangeEventHandler = this.onContentChangeEventHandler.bind(this);
		this.onSubmitEventHandler = this.onSubmitEventHandler.bind(this);
	}

	onTitleChangeEventHandler(event) {
		this.setState(() => {
			if (event.target.value.length <= 50) {
				return {
					title: event.target.value,
					titleLength: event.target.value.length,
				};
			} else {
				alert('Title must be less than 50 characters!')
				return {
					title: event.target.value,
				};
			}
		});
	}

	onContentChangeEventHandler(event) {
		this.setState(() => {
			return {
				body: event.target.value,
			};
		});
	}

	onSubmitEventHandler(event) {
		event.preventDefault();
        this.props.addNote(this.state);
		this.setState(() => {
			return {
				title: '',
				body: '',
				titleLength: 0,
			};
		});
	}

	render() {
		return (
			<div className="note-input" onSubmit={this.onSubmitEventHandler}>
				<h2>Make A Note...</h2>
				<form>
					<p className="note-input__title__char-limit">
						Character Left: {50 - this.state.titleLength}
					</p>
					<input
						type="text"
						placeholder="Title"
						value={this.state.title}
						onChange={this.onTitleChangeEventHandler}
					/>
					<textarea
						type="text"
						placeholder="Content"
						value={this.state.body}
						onChange={this.onContentChangeEventHandler}
					/>
					<button type="submit">Add Notes</button>
				</form>
			</div>
		)
	}
}

export default NotesInput;
