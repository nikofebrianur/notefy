import React from 'react';

function SearchBar({ onSearch }) {
	return (
		<div className='search-bar'>
			<div className='search-icon'>
			</div>
			<input 
				type='text' 
				className='search-input'
				placeholder='What note you want to look for?'
				onChange={(event) => onSearch(event.target.value)}
			/>
		</div>
	)
}

export default SearchBar;