import React from "react";
import { showFormattedDate } from "../utils/data";

function NotesItem({
	title,
	createdAt,
	body,
	id,
	isArchived,
	onArchive,
	onDelete
}) {
	if (isArchived) {
		return (
			<div className="note-item">
				<div className="note-item__content">
					<h3 className="note-item__title">{title}</h3>
					<p className="note-item__date">{showFormattedDate(createdAt)}</p>
					<p className="note-item__body">{body}</p>
				</div>
				<div className="note-item__action">
					<button className="note-item__delete-button" onClick={() => onDelete(id)}
					>Delete
						<i class="fa-solid fa-delete-left"></i>
					</button>
					<button className="note-item__archive-button" onClick={() => onArchive(id)}>Unarchive
					<i class="fa-solid fa-box-archive"></i>
					</button>
				</div>
			</div>
		)
	} else {
		return (
			<div className="note-item">
				<div className="note-item__content">
					<h3 className="note-item__title">{title}</h3>
					<p className="note-item__date">{showFormattedDate(createdAt)}</p>
					<p className="note-item__body">{body}</p>
				</div>
				<div className="note-item__action">
					<button className="note-item__delete-button" onClick={() => onDelete(id)} >Delete
						<i class="fa-solid fa-delete-left"></i>
					</button>
					<button className="note-item__archive-button" onClick={() => onArchive(id)}>Archive
						<i class="fa-solid fa-box-archive"></i>
					</button>
				</div>
			</div>
		)
	}
}

export default NotesItem;