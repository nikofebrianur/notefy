import React from 'react';
import { getInitialData } from '../utils/data';
import NavBar from './NavBar';
import NotesInput from './NotesInput';
import Main from "./Main";

class NotesApp extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			notes: getInitialData(),
			unfilteredNotes: getInitialData(),
		}

		this.onSearchHandler = this.onSearchHandler.bind(this);
		this.onAddNoteHandler = this.onAddNoteHandler.bind(this);
		this.onArchiveHandler = this.onArchiveHandler.bind(this);
		this.onDeleteHandler = this.onDeleteHandler.bind(this);
	}

	onAddNoteHandler({ title, body }) {
		this.setState((prevState) => {
			return {
				notes: [
					...prevState.notes,
					{
						id: +new Date(),
						title,
						createdAt: +new Date(),
						body,
						isArchived: false,
					},
				],

				unfilteredNotes: [
					...prevState.unfilteredNotes,
					{
						id: +new Date(),
						title,
						createdAt: +new Date(),
						body,
						isArchived: false,
					},
				],
			};
		});
	}

	onArchiveHandler(id) {
		this.setState((prevState) => {
			return {
				notes: prevState.notes.map((note) => {
					if (note.id === id) {
						return {
							...note,
							isArchived: !note.isArchived,
						}
					} else {
						return note;
					}
				}),

				unfilteredNotes: prevState.unfilteredNotes.map((note) => {
					if (note.id === id) {
						return {
							...note,
							isArchived:!note.isArchived,
						}
					} else {
						return note;
					}
				}),
			};
		});
	}

	onDeleteHandler(id) {
		const confirm = window.confirm('Are you sure you want to delete this note?');

		if (confirm) {
			this.setState((prevState) => {
				return {
					notes: prevState.notes.filter((note) => note.id !== id),
					unfilteredNotes: prevState.unfilteredNotes.filter((note) => note.id !== id),
				};
			});
		} else {
			return;
		}
	}

	onSearchHandler(keyword) {
		if (keyword.length > 0) {
			const undisplayInput = document.getElementsByClassName('note-input');
			undisplayInput[0].style.display = 'none';
			this.setState({
				notes: this.state.unfilteredNotes.filter((note) => note.title.toLowerCase().includes(keyword.toLowerCase())
				),
			})
		} else {
			const displayedInput = document.getElementsByClassName('note-input');
			displayedInput[0].style.display = '';
			this.setState({
				notes: this.state.unfilteredNotes,
			});
		}
	}

	render() {
		return (
			<div className='note-app'>
				<NavBar onSearch={(searchInput) => this.onSearchHandler(searchInput)} />
				<NotesInput addNote={this.onAddNoteHandler} />
				<Main
					notes={this.state.notes}
					onArchive = {this.onArchiveHandler}
					onDelete = {this.onDeleteHandler}
				/>
			</div>
		)
	}
}

export default NotesApp;