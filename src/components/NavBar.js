import React from 'react';
import SearchBar from './SearchBar';

function NavBar({ onSearch }) {
	return (
		<div className='note-app__header'>
			<h1 className='logo'>Notefy</h1>
			<SearchBar onSearch={onSearch} />
		</div>
	)
}

export default NavBar;